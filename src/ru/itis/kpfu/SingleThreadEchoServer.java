package ru.itis.kpfu;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;


public class SingleThreadEchoServer {
    public static void main(String[] args) {
        try{
            ServerSocket s = new ServerSocket(8189);
            Socket incoming = s.accept();

            InputStream inStream = incoming.getInputStream();
            OutputStream outStream = incoming.getOutputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(inStream));
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(outStream));

            try{
                out.write("Привет! Отправь BYE чтобы завершить работу.\n");
                out.flush();
                System.out.println("...connected to "+incoming.getInetAddress()+":"+incoming.getLocalPort());

                String line;
                while (((line = in.readLine()) != null) && !line.trim().equals("BYE")){
                    out.write("Эхо: " + line + "\n");
                    out.flush();
                    System.out.println("log: "+line);
                }
            } finally{
                System.out.println("Клиент был закрыт...");
                in.close();
                out.close();
                incoming.close();
                s.close();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
